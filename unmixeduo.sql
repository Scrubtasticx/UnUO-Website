-- --------------------------------------------------------
-- Host:                         glaze-tech.net
-- Server version:               5.5.42-cll-lve - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for UnmixedUO
DROP DATABASE IF EXISTS `UnmixedUO`;
CREATE DATABASE IF NOT EXISTS `UnmixedUO` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `UnmixedUO`;


-- Dumping structure for table UnmixedUO.donate_trans
DROP TABLE IF EXISTS `donate_trans`;
CREATE TABLE IF NOT EXISTS `donate_trans` (
  `id` varchar(255) COLLATE utf8_bin NOT NULL,
  `state` varchar(255) COLLATE utf8_bin NOT NULL,
  `account` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `credit` bigint(20) NOT NULL,
  `time` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8_bin,
  `extra` text COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `state` (`state`),
  KEY `account` (`account`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table UnmixedUO.donate_trans: 0 rows
DELETE FROM `donate_trans`;
/*!40000 ALTER TABLE `donate_trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `donate_trans` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.gift_type
DROP TABLE IF EXISTS `gift_type`;
CREATE TABLE IF NOT EXISTS `gift_type` (
  `type_id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(200) NOT NULL,
  `class_name` varchar(60) NOT NULL,
  `price` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.gift_type: 10 rows
DELETE FROM `gift_type`;
/*!40000 ALTER TABLE `gift_type` DISABLE KEYS */;
INSERT INTO `gift_type` (`type_id`, `type_name`, `class_name`, `price`) VALUES
	(1, '225 Stat Ball', 'StatBall', 100),
	(2, 'Ethereal Horse', 'EtherealHorse', 150),
	(3, 'Ethereal Beetle', 'EtherealBeetle', 150),
	(4, 'Ethereal Ostard', 'EtherealOstard', 150),
	(5, 'Ethereal Ridgeback', 'EtherealRidgeback', 150),
	(6, 'Ethereal Swamp Dragon', 'EtherealSwampDragon', 150),
	(7, 'Ethereal Llama', 'EtherealLlama', 200),
	(8, 'Ethereal Ki-rin', 'EtherealKirin', 200),
	(9, 'Ethereal Unicorn', 'EtherealUnicorn', 200),
	(10, 'Ridable Polar Bear', 'RideablePolarBear', 400);
/*!40000 ALTER TABLE `gift_type` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.myrunuo_characters
DROP TABLE IF EXISTS `myrunuo_characters`;
CREATE TABLE IF NOT EXISTS `myrunuo_characters` (
  `char_id` int(10) unsigned NOT NULL,
  `char_name` varchar(255) NOT NULL,
  `char_str` smallint(5) unsigned NOT NULL,
  `char_dex` smallint(6) NOT NULL,
  `char_int` smallint(6) NOT NULL,
  `char_female` tinyint(4) NOT NULL,
  `char_counts` smallint(6) NOT NULL,
  `char_guild` varchar(255) DEFAULT NULL,
  `char_guildtitle` varchar(255) DEFAULT NULL,
  `char_nototitle` varchar(255) DEFAULT NULL,
  `char_bodyhue` smallint(5) unsigned DEFAULT NULL,
  `char_public` tinyint(4) NOT NULL,
  PRIMARY KEY (`char_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.myrunuo_characters: ~22 rows (approximately)
DELETE FROM `myrunuo_characters`;
/*!40000 ALTER TABLE `myrunuo_characters` DISABLE KEYS */;
INSERT INTO `myrunuo_characters` (`char_id`, `char_name`, `char_str`, `char_dex`, `char_int`, `char_female`, `char_counts`, `char_guild`, `char_guildtitle`, `char_nototitle`, `char_bodyhue`, `char_public`) VALUES
	(1, 'Zycron Hoe Two', 60, 15, 15, 0, 0, 'NULL', 'NULL', 'Zycron Hoe Two', 33770, 0),
	(334, 'a curse troll', 37, 13, 42, 0, 25, '1', 'I like xanax', 'a curse troll', 33770, 1),
	(347, 'ImpulseX', 100, 25, 100, 1, 0, '1', 'I like xanax', 'ImpulseX', 33770, 0),
	(362, 'Im So Hood', 100, 100, 25, 0, 0, '1', 'I like xanax', 'The Rude Im So Hood', 33770, 0),
	(363, 'Get Shat Apon', 100, 100, 25, 0, 0, '1', 'I like xanax', 'Get Shat Apon', 33770, 0),
	(413, 'Gizmo the Grimey', 100, 25, 100, 0, 0, 'NULL', 'NULL', 'Gizmo the Grimey', 33810, 0),
	(467, 'Jack', 100, 75, 50, 0, 0, 'NULL', 'NULL', 'Jack', 33770, 0),
	(493, 'Zycron Hoe One', 100, 25, 100, 0, 0, '2', 'NULL', 'Zycron Hoe One', 33770, 0),
	(499, 'Warlorn Hoe One', 100, 25, 100, 0, 0, 'NULL', 'NULL', 'Warlorn Hoe One', 33770, 0),
	(551, 'Fortify', 100, 25, 100, 0, 0, '1', 'I like xanax', 'Fortify', 33770, 0),
	(552, 'Fortify', 100, 25, 100, 0, 0, 'NULL', 'NULL', 'Fortify', 33770, 0),
	(556, 'Zyc Gather Hoe', 100, 25, 100, 0, 0, 'NULL', 'NULL', 'Zyc Gather Hoe', 33770, 0),
	(572, 'a curse troll', 26, 25, 72, 0, 25, '1', 'I like xanax', 'a curse troll', 33770, 1),
	(610, 'Warlorn Hoe Two', 60, 15, 15, 0, 0, 'NULL', 'NULL', 'Warlorn Hoe Two', 33770, 0),
	(616, 'War Gather Hoe', 100, 25, 100, 0, 0, 'NULL', 'NULL', 'War Gather Hoe', 33770, 0),
	(1123, 'Zycron', 100, 25, 100, 0, 0, '1', 'I like Xanax', 'Zycron', 33770, 0),
	(1128, 'Warlorn', 100, 25, 100, 0, 1, '2', 'I like xanax', 'The Rude Warlorn', 33770, 0),
	(4205, 'a panther', 100, 25, 100, 0, 0, '2', 'I like xanax', 'a panther', 33770, 0),
	(8766, 'test', 25, 20, 45, 0, 0, 'NULL', 'NULL', 'test', 33770, 0),
	(11142, 'Lurka', 100, 25, 100, 0, 0, 'NULL', 'NULL', 'Lurka', 33770, 1),
	(17204, 'hinj', 25, 20, 45, 0, 0, 'NULL', 'NULL', 'hinj', 33770, 0),
	(95226, 'dat nikka Craig', 100, 100, 25, 0, 0, 'NULL', 'NULL', 'dat nikka Craig', 33826, 0);
/*!40000 ALTER TABLE `myrunuo_characters` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.myrunuo_characters_layers
DROP TABLE IF EXISTS `myrunuo_characters_layers`;
CREATE TABLE IF NOT EXISTS `myrunuo_characters_layers` (
  `char_id` int(10) unsigned NOT NULL,
  `layer_id` tinyint(3) unsigned NOT NULL,
  `item_id` smallint(5) unsigned NOT NULL,
  `item_hue` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`char_id`,`layer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.myrunuo_characters_layers: ~93 rows (approximately)
DELETE FROM `myrunuo_characters_layers`;
/*!40000 ALTER TABLE `myrunuo_characters_layers` DISABLE KEYS */;
INSERT INTO `myrunuo_characters_layers` (`char_id`, `layer_id`, `item_id`, `item_hue`) VALUES
	(1, 0, 5433, 808),
	(1, 1, 5903, 1752),
	(1, 2, 8059, 842),
	(1, 3, 5435, 1717),
	(1, 4, 5121, 0),
	(1, 5, 8251, 1102),
	(334, 0, 8251, 1102),
	(347, 0, 3834, 0),
	(347, 1, 8252, 1102),
	(362, 0, 5054, 2406),
	(362, 1, 5901, 1157),
	(362, 2, 8059, 1932),
	(362, 3, 5398, 1932),
	(362, 4, 5139, 2406),
	(362, 5, 5435, 1157),
	(362, 6, 5140, 2406),
	(362, 7, 5177, 2425),
	(362, 8, 5138, 2406),
	(363, 0, 8251, 1102),
	(413, 0, 5397, 1109),
	(413, 1, 7933, 464),
	(413, 2, 5422, 236),
	(413, 3, 5903, 1751),
	(413, 4, 7939, 1320),
	(413, 5, 3834, 0),
	(413, 6, 5912, 0),
	(467, 0, 5399, 432),
	(467, 1, 5422, 194),
	(467, 2, 5903, 1706),
	(467, 3, 5062, 0),
	(467, 4, 7939, 1328),
	(467, 5, 3834, 0),
	(467, 6, 5912, 0),
	(467, 7, 8257, 1102),
	(493, 0, 8251, 1102),
	(499, 0, 8251, 1102),
	(551, 0, 3834, 0),
	(551, 1, 8251, 1102),
	(552, 0, 3834, 0),
	(552, 1, 8251, 1102),
	(556, 0, 8251, 1102),
	(572, 0, 3834, 0),
	(572, 1, 8251, 1102),
	(610, 0, 5399, 217),
	(610, 1, 5433, 849),
	(610, 2, 5903, 1714),
	(610, 3, 5435, 1730),
	(610, 4, 5119, 0),
	(610, 5, 8251, 1102),
	(616, 0, 8251, 1102),
	(1123, 0, 3834, 0),
	(1123, 1, 8251, 1102),
	(1128, 0, 7933, 1916),
	(1128, 1, 5901, 1916),
	(1128, 2, 8059, 1932),
	(1128, 3, 5398, 1932),
	(1128, 4, 5435, 1916),
	(1128, 5, 5440, 1938),
	(4205, 0, 7933, 1916),
	(4205, 1, 5903, 1916),
	(4205, 2, 8059, 1932),
	(4205, 3, 5398, 1932),
	(4205, 4, 5435, 1916),
	(4205, 5, 5911, 1916),
	(8766, 0, 7933, 241),
	(8766, 1, 5433, 461),
	(8766, 2, 5903, 1754),
	(8766, 3, 5062, 0),
	(8766, 4, 7939, 1304),
	(8766, 5, 3834, 0),
	(8766, 6, 5912, 0),
	(11142, 0, 5422, 259),
	(11142, 1, 5903, 1738),
	(11142, 2, 8059, 242),
	(11142, 3, 5062, 0),
	(11142, 4, 7939, 1329),
	(11142, 5, 3834, 0),
	(11142, 6, 5912, 0),
	(17204, 0, 5433, 861),
	(17204, 1, 5903, 1722),
	(17204, 2, 8059, 121),
	(17204, 3, 5062, 0),
	(17204, 4, 7939, 1313),
	(17204, 5, 3834, 0),
	(17204, 6, 5912, 0),
	(95226, 0, 5054, 2219),
	(95226, 1, 5903, 1750),
	(95226, 2, 8059, 220),
	(95226, 3, 5139, 2219),
	(95226, 4, 5140, 2219),
	(95226, 5, 5177, 2219),
	(95226, 6, 5138, 2219),
	(95226, 7, 8269, 1102);
/*!40000 ALTER TABLE `myrunuo_characters_layers` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.myrunuo_characters_skills
DROP TABLE IF EXISTS `myrunuo_characters_skills`;
CREATE TABLE IF NOT EXISTS `myrunuo_characters_skills` (
  `char_id` int(10) unsigned NOT NULL,
  `skill_id` tinyint(4) NOT NULL,
  `skill_value` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`char_id`,`skill_id`),
  KEY `skill_id` (`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.myrunuo_characters_skills: ~138 rows (approximately)
DELETE FROM `myrunuo_characters_skills`;
/*!40000 ALTER TABLE `myrunuo_characters_skills` DISABLE KEYS */;
INSERT INTO `myrunuo_characters_skills` (`char_id`, `skill_id`, `skill_value`) VALUES
	(1, 4, 300),
	(1, 7, 500),
	(1, 37, 500),
	(1, 45, 300),
	(1, 46, 4),
	(334, 23, 542),
	(334, 25, 510),
	(334, 26, 500),
	(334, 46, 8),
	(347, 0, 1000),
	(347, 16, 1000),
	(347, 21, 1000),
	(347, 25, 1000),
	(347, 26, 1000),
	(347, 30, 1000),
	(347, 46, 1000),
	(362, 0, 1000),
	(362, 1, 1000),
	(362, 17, 1000),
	(362, 21, 1000),
	(362, 26, 1000),
	(362, 27, 1000),
	(362, 41, 1000),
	(363, 0, 1000),
	(363, 1, 1000),
	(363, 17, 1000),
	(363, 26, 1000),
	(363, 27, 1000),
	(363, 40, 1000),
	(363, 44, 1000),
	(413, 14, 1000),
	(413, 21, 1000),
	(413, 25, 1000),
	(413, 28, 1000),
	(413, 33, 1000),
	(413, 46, 1000),
	(413, 47, 1000),
	(467, 1, 1000),
	(467, 8, 1000),
	(467, 17, 1000),
	(467, 23, 1000),
	(467, 25, 1000),
	(467, 27, 1000),
	(467, 31, 1000),
	(493, 7, 1200),
	(493, 8, 1000),
	(493, 11, 1000),
	(493, 25, 1000),
	(493, 29, 1000),
	(493, 34, 1000),
	(493, 37, 1000),
	(499, 7, 1000),
	(499, 11, 1000),
	(499, 12, 1000),
	(499, 25, 1000),
	(499, 29, 1000),
	(499, 34, 1000),
	(499, 37, 1000),
	(551, 2, 1000),
	(551, 16, 1000),
	(551, 25, 1000),
	(551, 26, 1000),
	(551, 35, 1000),
	(551, 39, 1000),
	(551, 46, 1000),
	(552, 2, 1000),
	(552, 16, 1000),
	(552, 25, 1000),
	(552, 26, 1000),
	(552, 35, 1000),
	(552, 39, 1000),
	(552, 46, 1000),
	(556, 3, 1000),
	(556, 13, 1000),
	(556, 18, 1000),
	(556, 25, 1000),
	(556, 44, 1000),
	(556, 45, 1000),
	(556, 46, 1000),
	(572, 0, 1000),
	(572, 25, 200),
	(572, 26, 500),
	(572, 46, 7),
	(610, 4, 300),
	(610, 7, 500),
	(610, 37, 500),
	(610, 45, 300),
	(610, 46, 3),
	(616, 3, 1000),
	(616, 4, 1000),
	(616, 18, 1000),
	(616, 25, 1000),
	(616, 44, 1000),
	(616, 45, 1000),
	(616, 46, 1000),
	(1123, 1, 1000),
	(1123, 16, 1000),
	(1123, 25, 1000),
	(1123, 26, 1000),
	(1123, 30, 1000),
	(1123, 43, 1000),
	(1123, 46, 1000),
	(1128, 0, 1000),
	(1128, 1, 1000),
	(1128, 16, 1000),
	(1128, 25, 1000),
	(1128, 26, 1000),
	(1128, 43, 1000),
	(1128, 46, 1000),
	(4205, 16, 1000),
	(4205, 21, 1000),
	(4205, 25, 1000),
	(4205, 26, 1000),
	(4205, 33, 1000),
	(4205, 46, 1000),
	(4205, 47, 1000),
	(8766, 16, 300),
	(8766, 25, 500),
	(8766, 43, 300),
	(8766, 46, 500),
	(11142, 16, 1000),
	(11142, 23, 1000),
	(11142, 25, 1000),
	(11142, 26, 1000),
	(11142, 32, 1000),
	(11142, 43, 1000),
	(11142, 49, 1000),
	(17204, 16, 300),
	(17204, 25, 500),
	(17204, 43, 300),
	(17204, 46, 500),
	(95226, 1, 1000),
	(95226, 17, 1000),
	(95226, 21, 1000),
	(95226, 25, 1000),
	(95226, 26, 1000),
	(95226, 27, 1000),
	(95226, 41, 1000);
/*!40000 ALTER TABLE `myrunuo_characters_skills` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.myrunuo_guilds
DROP TABLE IF EXISTS `myrunuo_guilds`;
CREATE TABLE IF NOT EXISTS `myrunuo_guilds` (
  `guild_id` smallint(5) unsigned NOT NULL,
  `guild_name` varchar(255) NOT NULL,
  `guild_abbreviation` varchar(31) DEFAULT NULL,
  `guild_website` varchar(255) DEFAULT NULL,
  `guild_charter` varchar(255) DEFAULT NULL,
  `guild_type` varchar(8) NOT NULL,
  `guild_wars` smallint(5) unsigned NOT NULL,
  `guild_members` smallint(5) unsigned NOT NULL,
  `guild_master` int(10) unsigned NOT NULL,
  PRIMARY KEY (`guild_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.myrunuo_guilds: ~2 rows (approximately)
DELETE FROM `myrunuo_guilds`;
/*!40000 ALTER TABLE `myrunuo_guilds` DISABLE KEYS */;
INSERT INTO `myrunuo_guilds` (`guild_id`, `guild_name`, `guild_abbreviation`, `guild_website`, `guild_charter`, `guild_type`, `guild_wars`, `guild_members`, `guild_master`) VALUES
	(1, 'Guts &amp; Glory', 'G&amp;G', NULL, NULL, 'Standard', 0, 7, 1123),
	(2, 'Guts &amp; Gory Factions', 'GnG', NULL, NULL, 'Standard', 0, 3, 4205);
/*!40000 ALTER TABLE `myrunuo_guilds` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.myrunuo_guilds_wars
DROP TABLE IF EXISTS `myrunuo_guilds_wars`;
CREATE TABLE IF NOT EXISTS `myrunuo_guilds_wars` (
  `guild_1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `guild_2` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guild_1`,`guild_2`),
  KEY `guild1` (`guild_1`),
  KEY `guild2` (`guild_2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.myrunuo_guilds_wars: ~0 rows (approximately)
DELETE FROM `myrunuo_guilds_wars`;
/*!40000 ALTER TABLE `myrunuo_guilds_wars` DISABLE KEYS */;
/*!40000 ALTER TABLE `myrunuo_guilds_wars` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.myrunuo_status
DROP TABLE IF EXISTS `myrunuo_status`;
CREATE TABLE IF NOT EXISTS `myrunuo_status` (
  `char_id` int(12) unsigned NOT NULL DEFAULT '0',
  `char_location` varchar(14) DEFAULT NULL,
  `char_map` varchar(8) DEFAULT NULL,
  `char_karma` int(6) DEFAULT NULL,
  `char_fame` int(6) DEFAULT NULL,
  PRIMARY KEY (`char_id`),
  KEY `charid` (`char_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.myrunuo_status: ~0 rows (approximately)
DELETE FROM `myrunuo_status`;
/*!40000 ALTER TABLE `myrunuo_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `myrunuo_status` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.myrunuo_timestamps
DROP TABLE IF EXISTS `myrunuo_timestamps`;
CREATE TABLE IF NOT EXISTS `myrunuo_timestamps` (
  `time_datetime` varchar(22) DEFAULT '',
  `time_type` varchar(6) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.myrunuo_timestamps: ~0 rows (approximately)
DELETE FROM `myrunuo_timestamps`;
/*!40000 ALTER TABLE `myrunuo_timestamps` DISABLE KEYS */;
/*!40000 ALTER TABLE `myrunuo_timestamps` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.paypal_processed_txn
DROP TABLE IF EXISTS `paypal_processed_txn`;
CREATE TABLE IF NOT EXISTS `paypal_processed_txn` (
  `txn_id` varchar(25) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`txn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.paypal_processed_txn: 0 rows
DELETE FROM `paypal_processed_txn`;
/*!40000 ALTER TABLE `paypal_processed_txn` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_processed_txn` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.paypal_transaction
DROP TABLE IF EXISTS `paypal_transaction`;
CREATE TABLE IF NOT EXISTS `paypal_transaction` (
  `mc_gross` double unsigned NOT NULL,
  `protection_eligibility` varchar(20) NOT NULL,
  `payer_id` varchar(20) NOT NULL,
  `tax` double unsigned NOT NULL DEFAULT '0',
  `payment_date` varchar(30) NOT NULL,
  `payment_status` varchar(20) NOT NULL,
  `charset` varchar(30) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `option_selection1` varchar(40) DEFAULT NULL,
  `notify_version` varchar(8) NOT NULL,
  `custom` varchar(80) DEFAULT NULL,
  `payer_status` varchar(20) NOT NULL,
  `business` varchar(80) NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `verify_sign` varchar(70) NOT NULL,
  `payer_email` varchar(80) NOT NULL,
  `option_name1` varchar(40) DEFAULT NULL,
  `txn_id` varchar(25) NOT NULL,
  `payment_type` varchar(25) NOT NULL,
  `btn_id` mediumint(8) unsigned NOT NULL,
  `last_name` varchar(60) DEFAULT NULL,
  `receiver_email` varchar(80) NOT NULL,
  `shipping_discount` double unsigned NOT NULL DEFAULT '0',
  `insurance_amount` double unsigned NOT NULL DEFAULT '0',
  `receiver_id` varchar(20) NOT NULL,
  `pending_reason` varchar(50) NOT NULL,
  `txn_type` varchar(50) NOT NULL,
  `item_name` varchar(80) NOT NULL,
  `discount` double unsigned NOT NULL DEFAULT '0',
  `mc_currency` varchar(5) NOT NULL,
  `item_number` int(10) unsigned NOT NULL,
  `residence_country` varchar(5) NOT NULL,
  `test_ipn` bit(1) NOT NULL,
  `receipt_id` varchar(30) NOT NULL,
  `handling_amount` double NOT NULL DEFAULT '0',
  `shipping_method` varchar(20) NOT NULL,
  `transaction_subject` varchar(80) NOT NULL,
  `payment_gross` double DEFAULT NULL,
  `shipping` double NOT NULL DEFAULT '0',
  `mc_fee` double unsigned NOT NULL DEFAULT '0',
  `payment_fee` double unsigned DEFAULT NULL,
  `invoice` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`txn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.paypal_transaction: 0 rows
DELETE FROM `paypal_transaction`;
/*!40000 ALTER TABLE `paypal_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `paypal_transaction` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.redeemable_gift
DROP TABLE IF EXISTS `redeemable_gift`;
CREATE TABLE IF NOT EXISTS `redeemable_gift` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` smallint(5) unsigned NOT NULL,
  `account_name` varchar(60) NOT NULL,
  `donate_time` int(11) unsigned NOT NULL,
  `paypal_txn_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_name` (`account_name`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.redeemable_gift: 0 rows
DELETE FROM `redeemable_gift`;
/*!40000 ALTER TABLE `redeemable_gift` DISABLE KEYS */;
/*!40000 ALTER TABLE `redeemable_gift` ENABLE KEYS */;


-- Dumping structure for table UnmixedUO.redeemed_gift
DROP TABLE IF EXISTS `redeemed_gift`;
CREATE TABLE IF NOT EXISTS `redeemed_gift` (
  `id` bigint(20) unsigned NOT NULL,
  `type_id` smallint(5) unsigned NOT NULL,
  `account_name` varchar(60) NOT NULL,
  `donate_time` int(11) unsigned NOT NULL,
  `redeem_time` int(11) unsigned NOT NULL,
  `serial` varchar(80) NOT NULL,
  `paypal_txn_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `account_name` (`account_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table UnmixedUO.redeemed_gift: 8 rows
DELETE FROM `redeemed_gift`;
/*!40000 ALTER TABLE `redeemed_gift` DISABLE KEYS */;
INSERT INTO `redeemed_gift` (`id`, `type_id`, `account_name`, `donate_time`, `redeem_time`, `serial`, `paypal_txn_id`) VALUES
	(1, 1, 'scrubtasticx', 1421192738, 1421174748, '0x40000267', '0000000000'),
	(2, 1, 'scrubtasticx', 1421435931, 1421410590, '0x40000266', '0000000000'),
	(3, 1, 'scrubtasticx', 1421437006, 1421411669, '0x40000267', '0000000000'),
	(4, 1, 'zeropa4', 1421437762, 1421412428, '0x40001465', '0000000000'),
	(5, 1, 'zycron', 1421457500, 1421432157, '0x40000FFC', '0000000000'),
	(6, 8, 'zycron', 1438289378, 1438264409, '0x40108959', '0000000000'),
	(7, 1, 'scrubtasticx', 1439569392, 1439544221, '0x40027BCA', '0000000000'),
	(8, 3, 'scrubtasticx', 1439569400, 1439544225, '0x40027BCB', '0000000000');
/*!40000 ALTER TABLE `redeemed_gift` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
